[![build status](https://gitlab.science.gc.ca/CanESM/CanESM5/badges/develop_canesm/build.svg)](https://gitlab.science.gc.ca/CanESM/CanESM5/commits/develop_canesm)

# CanESM: The Canadian Earth System Model

CanESM is a fully coupled Earth System Model, developed at the *[Canadian Centre for Climate Modelling and Analysis](https://ec.gc.ca/ccmac-cccma/)*, 
Climate Research Division, [Environment and Climate Change Canada](https://www.canada.ca/en/environment-climate-change.html) (ECCC). This respository includes all the source code, utilities and scripts used to compile and run CanESM5 and to do diagnostics on ECCC's High Performance Computers
(see disclaimer regarding usage below; excludes 3rd party dependencies such as NetCDF).


The CanESM code is made openly available at [gitlab.com/cccma/canesm](https://gitlab.com/cccma/canesm). An internal version of the code is also maintained on the [ECCC gitlab server](https://gitlab.science.gc.ca/CanESM/CanESM5), and includes 
[issue tracking](https://gitlab.science.gc.ca/CanESM/CanESM5/issues) and a [wiki](https://gitlab.science.gc.ca/CanESM/CanESM5/wikis/home). This is only available from within 
the ECCC/Science networks, and requires a science.gc.ca login (same as for science.gc.ca machines).


A detailed description and evaluation of CanESM version 5 maybe found in the open access [CanESM5 special issue of Geoscientific Model Development](https://www.geosci-model-dev.net/special_issue989.html). See the docs/ folder for a more detailed user guide.

## Layout

This CanESM5 repository is referred to as the *super-repo*. There are six directories at the top level of the super-repo:

 - CanAM       : The Canadian Atmosphere Model source code (also see CanDIAG). This is code strictly used by the model alone.

 - CanDIAG     : The AGCM diagnostics code, also including all code which overlaps between diagnostics and CanAM. 

 - CanCPL      : The "new" coupler, built for CanAM-CanNEMO.

 - CanNEMO     : The CCCma configuration of NEMO.

 - CCCma_tools : All scripts and utilities, which effectively make up the "CCCma environment".

 - CONFIG      : Example make_jobs and basefiles (this is not a submodule).
 
The first five directories (CanAM, CanDIAG, CanCPL, CanNEMO and CCCma_tools) are git *submodules*. That means each of these directories is an independent git repository,
and is tracked by git in the super-repo. The code in each submodule and the super-repo must be treated as separate repositories, as described in the development instructions below. 
The purpose of the super-repo is to keep track of which versions of the (submodule) code define a consistent and working version of the ESM (or AMIP configuration). Hence, a version of the 
ESM is completely defined by a commit (SHA1 checksum) or tag of the super-repo.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/canesm/badge/?version=latest)](https://canesm.readthedocs.io/en/latest/?badge=latest)

## License

CanESM is distributed under the [Open Government License - Canada version 2.0](https://open.canada.ca/en/open-government-licence-canada)

## Support disclaimer

This code is made available on an as-is basis. lt has been tested only on the computing facilities
within Environment and Climate Change Canada (ECCC). There is no guarantee that it will run on
other platforms or if it does, that it will run correctly. No support of any kind will be made available
to help users to run the model on their own system. README documents linked below describe the development 
process  used on ECCC machines.

## Running and modifying CanESM from Version Control (ECCC machines only!)

- Running instructions: [Running\_readme.md](Running_readme.md).

- Developing instructions:  [Developing\_readme.md](Developing_readme.md). 

- Diagnostics instructions: [Diagnostics\_readme.md](Diagnostics_readme.md)


