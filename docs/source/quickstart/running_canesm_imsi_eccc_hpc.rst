******************************************************** 
 Running CanESM on ECCC U2 using imsi
********************************************************

Overview and resources
######################

This document provides instructions for running the CanESM5 model on the
ECCC U2 system. This guide is relying on a model configuration system in 
development called *imsi*, which is in alpha testing to replace config-canesm.
You should use the ppp's for imsi work (for now).

Accessing IMSI Commands
-------------------------

To interface with imsi, we provide group cli environments to call imsi functions from.

To activate the CLI environments

.. code-block:: bash

    source /home/scrd102/cccma_libs/imsi/pyenv_3.10_imsi-cli-pp_2024-08-30/bin/activate

You may also install imsi yourself (see `imsi repo <https://gitlab.com/cccma/imsi>`_).
Read the `imsi documentation <https://imsi.readthedocs.io/en/latest/?version=latest>`_ to get an
overview of how the tools work.

Setup a piControl run
#####################

Navigate to your scratch space (ords or sitestore is good since all code is kept here) and setup a new piControl run as follows [#f1]_:

.. code-block:: bash
    
    imsi setup --repo=https://gitlab.science.gc.ca/CanESM/CanESM5.git --ver=imsi-integration --exp=cmip6-piControl --model=canesm51_p1 --runid=<unique-runid>

where you replace `--ver` and `--exp` as needed. Be sure to specify a unique runid, that won't clash with other
users, and keep it below about 15 characters. Using initials is good. e.g. `--runid=ncs-hist-tst-01`


Next change into the run directory (named for <runid>) and build the executables:

.. code-block:: bash
    
    imsi build

Save the default restart files:

.. code-block:: bash
    
    ./save_restart_files.sh

Submit the run to the batch queue:

.. code-block:: bash
    
    imsi submit

This will launch the imsi maestro suite, which has the same structure as the regular maestro suite.
To monitor a run, you can `export SEQ_EXP_HOME=sequencer` and use `xflow` as per normal.

.. rubric:: Footnotes

.. [#f1] Note, using the `https` address for gitlab is used here as it has no additional requirements. In general using `--repo=git@gitlab.com:cccma/canesm.git` is superior, but requires `adding your ssh keys to gitlab <https://docs.gitlab.com/ee/user/ssh.html>`_. 
