This directory contains the output 'final.state' and 'agcm.final.state' files, for
reference ensembles produced using the ensemble files one directory up. These files contain
checksum representations of the prognostic state of the ocean ('final.state') and the atmosphere
('agcm.final.state') at the given dates, for the given run types, where the files are named accordingly:

    RUNTYPE.FLAGS.OUTPUTDATE.FILETYPE

        RUNTYPE     -> the type of run ran (see below)
        FLAGS       -> grouping of compiler flags used (repro/prod)
        OUTPUTDATE  -> the model date that output the given file
        FILETYPE    -> either final.state (ocean) or agcm.final.state (agcm)

Where RUNTYPE maps onto runmode/configs in the following manner:

    RUNTYPE         RUNMODE                     CONFIG
    omip        :   OMIP1                       OMIP
    adev        :   CanAM-Dev-2003-2008         AMIP
    mam-adev    :   CanAM-MAM-Dev-2003-2008     AMIP
    pict        :   NEMO-AGCM-pi                ESM
    pifree      :   NEMO-AGCM-relaxed-co2       ESM
    pirlx       :   NEMO-AGCM-pi-free-co2       ESM
    hist        :   NEMO-AGCM-hist              ESM
    ssp         :   NEMO-AGCM-ssp585            ESM
    rco2        :   NEMO-AGCM-hist-relaxed-co2  ESM
    fco2        :   NEMO-AGCM-hist-free-co2     ESM
    4xco2       :   NEMO-AGCM-4xCO2             ESM
    1ppy        :   NEMO-AGCM-1ppy              ESM
    dcpp        :   NEMO-AGCM-dcpp-assim        ESM
    init        :   CanESM-init                 ESM
    maminit     :   CanESM-dynMAM-init          ESM

and the two FLAGS options represent:

    prod    -> the default flags used for production simulations
    repro   -> more conservative flags that limit the amount of optimization the compiler might do,
                thereby improving reproducibility

Note that the CanAM-PAM runmode CAN be ran as part of the AMIP DECK+ ensemble, but there are currently
three issues:
    1. the PAM run leaves ~1TB of data on disk
    2. the year test takes 8+ hours to complete and
    3. bit reproducibility has not yet been confirmed for PAM
As such, users should not run this runmode in their default tests. However, during the review process,
developers may be asked to confirm that this runmode still works

For specifics on what restarts were used to produce these runs, see the reference ensemble tables.

***********************************************************
* For information on how to launch your own deck+ ensemble
***********************************************************
    - see https://gitlab.science.gc.ca/CanESM/CanESM5/wikis/how-to-launch-a-deck-plus-ensemble
