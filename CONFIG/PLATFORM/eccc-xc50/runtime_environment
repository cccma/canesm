#!/bin/bash
HOSTIDf=$(echo $HOSTID | sed -e 's/eccc.*-ppp/cs/g' -e 's/^ppp/cs/g' | cut -c 1-3) 
export HOSTIDf

#### === ESSENTIAL
# -- "big endian format conversion"
export F_UFMTENDIAN='big'
# -- likely for stack size per thread
export OMP_STACKSIZE=1G
# -- Increase stack size limit
ulimit -all
ulimit -s unlimited || :

#### === ADVANTAGEOUS
# -- enable "ifort" buffered I/O
export FORT_BUFFERED=TRUE # "-assume buffered_stdout"
export BUFFERED=yes #  "-assume buffered_io"
# -- beneficial with "-cc depth" use in "aprun" command
export KMP_AFFINITY=disabled 

#### === DESIRABLE
# -- Recipe to separate output by task_id:
export PMI_NO_FORK=1
export ALPS_STDOUTERR_SPEC=$(pwd)

# -- Potential workaround for hang in MPI_Init
export PMI_MMAP_SYNC_WAIT_TIME=180
export PMI_CONNECT_RETRIES=100

# export PMI_DEBUG=1

#### === USEFUL
# -- MPI Info 
export MPICH_VERSION_DISPLAY=1
export MPICH_ENV_DISPLAY=1
export MPICH_CPUMASK_DISPLAY=1
export MPICH_RANK_REORDER_DISPLAY=1

#### === MAY BE USEFUL; needs to be tested
# Controls the threshold for switching from eager to rendezvous protocols for inter-node messaging.
export MPICH_GNI_MAX_EAGER_MSG_SIZE=131072
