#!/bin/bash
# Diagnostic hack job deluxe
set -x
set -e

# Get common shell functions and run-specific diag parameters
source $CANESM_SRC_ROOT/CCCma_tools/tools/CanESM_shell_functions.sh
source ${WRK_DIR}/config/diag_parameters
source ${WRK_DIR}/config/.simulation.time.state

# Time variables need to be sorted out globally as they are a complete dogs breakfast 
export year_rtdiag_start=${run_start_year}
export month_rtdiag_start=${run_start_month}
export year=$DIAG_YEAR
export mon=01
export months_gcm=12
export rtd_dest_dir=$rtd_dest_dir
export canam_rtd_staging_dir=$canam_rtd_staging_dir
export cannemo_rtd_staging_dir=$cannemo_rtd_staging_dir
# Months for which to execute NEMO RTD
export start_mon=01  # These are derived with a complex function in the make diag job. Needs to be revisited with global timers.
export last_mon=12
export months_run=12
export months_gs="+`seq $start_mon $months_run $last_month | awk '{print ($1-1)%12+1}'|tr '\n' ','`"
export nemo_rtd_mons=01 #`echo $months_gs | sed 's/+//' | tr ',' ' ' | xargs printf "%02d "`
# This should be backported into the imsi setup
export RUNPATH=${RUNPATH}/$year
export uxxx=mc
export parallel_io=on
export run_prefix="mc_${runid}_"
export mon_rs=12
export tail_suffix="_"
# model/restart names
export model1=$( printf "%s%04d_m%02d%s" $run_prefix $year $mon )
export model_rs=$( printf "%s%04d_m%02d%s" $run_prefix $year $mon_rs $tail_suffix )
export job_start_year=$year
export job_start_month=01
export job_stop_year=$year
export job_stop_month=12
export months_gs=1
export AWK=awk
source ${CANESM_SRC_ROOT}/CONFIG/COMMON/basefile_defaults/agcm_diag_basefile_defaults.cfg
export uxxx=mc
export canesm_diag_decks='delmodinfo.dk gpintstat2.dk gsstats7.dk xstats5.dk xtrachem.dk xtraconv2.dk effrdsrbc1.dk aodpth_volc1.dk lakestats.dk sfctile.dk xtradust_bulk.dk tmstats.dk daily_cmip6.dk epflux_cmip6.dk gwdiag_cmip6.dk cosp4_monthly.dk cleanall.dk'

# These are the actual diagnostic shell scripts, submitted to run in parallel

export cwd=$PWD

if [ "$with_rbld_nemo" -eq 1 ]; then
    mkdir nemo_rebuild
    echo "Run nemo_rebuild:"
    ( cd nemo_rebuild ; source $CANESM_SRC_ROOT/CanNEMO/lib/nemo_rebuild.sh )
fi

if [ "$with_gcmpak" -eq 1 ]; then
    echo "Run gcmpak:"
    mkdir gcmpak
    ( cd gcmpak ;  ${CANESM_SRC_ROOT}/CanDIAG/diag4/gcmpak.sh )
fi

if [ "$with_rtd_nemo" -eq 1 ]; then
    echo "Run nemo RTD:"
    mkdir nemo_rtd
    (cd nemo_rtd ;  source ${CANESM_SRC_ROOT}/CanNEMO/diag/canesm_nemo_runtime_diag.sh )
fi

if [ "$with_rtd" -eq 1 ]; then
    echo "Run rtdiag74:"
    mkdir canam_rtd
    (cd canam_rtd ; ${CANESM_SRC_ROOT}/CanDIAG/diag4/rtdiag74.sh )
fi

if [ "$with_diag" -eq 1 ]; then
    echo "Run AGCM monthly diagnostics"
    mkdir canam_diag
    (cd canam_diag; source ${CANESM_SRC_ROOT}/CCCma_tools/maestro-suite/default-imsi/modules/postproc/diagnostics_loop/agcm_diag.tsk
    )
fi

if [ "$with_time_series" -eq 1 ]; then
    echo "Run AGCM file splitting"
    mkdir canam_split
    (cd canam_split; source ${CANESM_SRC_ROOT}/CCCma_tools/maestro-suite/default-imsi/modules/postproc/diagnostics_loop/split_agcm_files.tsk
    )
fi

if [ "$with_nemo_diag" -eq 1 ]; then
    echo "Run OGCM monthly diagnostics"
    mkdir ocean_diag
    (cd ocean_diag; source ${CANESM_SRC_ROOT}/CCCma_tools/maestro-suite/default-imsi/modules/postproc/diagnostics_loop/ocean_diag.tsk
    )
fi


if [ "$with_netcdf_conv" -eq 1 ]; then
    echo "Run netcdf conversion"
    #-- get the run conversion tables if not already created
    run_netcdf_conv_config_dir=${WRK_DIR}/config/netcdf_conv
    ncconv_dir=${CANESM_SRC_ROOT}/CCCma_tools/data_conv/ncconv
    if ! is_directory ${run_netcdf_conv_config_dir}; then
        # build up flag string
        flags="-a $CANESM_SRC_ROOT/CONFIG/COMMON/ncconv_cccma_json_files "
        is_defined $ncconv_override_license && flags="$flags -l $ncconv_override_license "

        # get and config tables
        ${ncconv_dir}/tools/config_conversion_tables $flags ncconv_dir=$ncconv_dir dest_dir=$run_netcdf_conv_config_dir\
                                                     conv_project=$ncconv_conv_project runid=$runid source_id=$source_id exp_id=$experiment_id\
                                                     activity_id=$activity_id subexp_id=$subexperiment_id
    fi

    #--- populate an experiment table for this run
    if ! is_file $ncconv_exptab; then
        # first populate a blank table
        mkdir -p $(dirname $ncconv_exptab)
        cp ${ncconv_exptab_template} $ncconv_exptab

        # produce the entry for this run and inject it into the table
        NEW_ENTRY=$(${ncconv_dir}/tools/create_experiment_entry runid=$runid run_start=$run_start_year run_stop=$run_stop_year\
                                                    variant_label=$variant_label parent_runid=$parent_runid parent_branch_time=$parent_branch_time\
                                                    source_id=$source_id experiment_id=$experiment_id activity_id=$activity_id\
                                                    subexperiment_id=${subexperiment_id})
        echo $NEW_ENTRY >> $ncconv_exptab
    fi

    #-- run the conversion!
    mkdir ncconv
    ( cd ncconv;\
      source ${CANESM_SRC_ROOT}/CCCma_tools/maestro-suite/default-imsi/modules/postproc/wrap_up_loop/netcdf_conv.tsk )
fi

# cleanup
source clean_run_files.sh

next_year=$(( $DIAG_YEAR + 1 ))
completed_year=$(( $CURRENT_YEAR - 1 ))
sed -i "s/DIAG_YEAR=.*/DIAG_YEAR=${next_year}/" ${WRK_DIR}/config/.simulation.time.state
sed -i "s/DIAG_RESUBMIT=.*/DIAG_RESUBMIT=FALSE/" ${WRK_DIR}/config/.simulation.time.state

# and will resubmit themselves until they are as close as possible to model time.
if [ "$next_year" -le "$END_YEAR" ] && [ "$next_year" -lt "$completed_year" ]; then
    echo "Resubmitting diagnostics for $NEXT_YEAR"
    sed -i "s/DIAG_RESUBMIT=.*/DIAG_RESUBMIT=TRUE/" ${WRK_DIR}/config/.simulation.time.state
fi
