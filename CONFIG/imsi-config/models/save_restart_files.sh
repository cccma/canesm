#!/bin/bash
set -e
# save restart files script
#==========================
# define usage message
Usage='
  Usage:
  save_restart_files [-a nl_file1] [-c nl_file2] [-n nl_file3]
                     [config_file=/path/to/file.cfg]
    Access saved restart files; process them and save for this run.
    Args:
        config_file : [optional] path to file containing the high level config settings
                        for this run (i.e. parent runid) defaults to `canesm.cfg` in the
                        working directory.
    Flags:
        -a : tells the script to pull the given agcm namelist file from the extracted agcm
             restart and use it for the run. Must be used multiple times if multple namelists are
             desired.
        -c : tells the script to pull the given coupler namelist file from the extracted coupler
             restart and use it for the run. Must be used multiple times if multiple namelists are
             desired.
        -n : tells the script to pull the given nemo namelist file from the extracted nemo
             restart and use it for the run. Must be used multiple times if multiple namelists are
             desired.
'
if [ -z "$agcm_restart_identifier" ]; then 
    agcm_restart_identifier=agcmrs
fi

config_file="config/shell_parameters" # unless user specifies this file assume it is present in calling directory
call_dir=$(pwd)
access_log_file="${call_dir}/.save_restarts_$$_access.log"

#~~~~~~~~~~~~~~~
# Parse Options
#~~~~~~~~~~~~~~~
# build arrays of namelist files to pull from the restarts and use for the run
while getopts a:n:c:h opt; do
    case $opt in
        a) agcm_restart_namelists_to_use+=("$OPTARG");;
        n) nemo_restart_namelists_to_use+=("$OPTARG");;
        c) coupler_restart_namelists_to_use+=("$OPTARG");;
        h) echo "$Usage"; exit 0;;
        *) echo "$Usage"; exit 1;;
    esac
done
shift $(( OPTIND - 1 ))
#~~~~~~~~~~~~~~~~~
# Parse Arguments
#~~~~~~~~~~~~~~~~~
for arg in "$@"; do
    case $arg in
        *=*)
            var=$(echo $arg | awk -F\= '{printf "%s",$1}')
            val=$(echo $arg | awk -F\= '{printf "%s",$2}')
            case $var in
                config_file) config_file="$val" ;; # file containing high level config settings
                *) echo "Invalid command line arg --> $arg <--"; exit 1 ;;
            esac ;;
        *) echo "Invalid command line arg --> $arg <--"; exit 1 ;;
    esac
done
[[ -z $config_file ]] && { echo "save_restart_files needs to know the config file"; exit 1;}
source ${config_file}

config=${model_config} 
echo
echo config: $config
echo 

# get useful shell functions
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Perform some sanity checks
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mkdir -p $RUNPATH
#~~~~~~~~~~~~~~~~~~~~~~~~~
# Set inherited variables
#~~~~~~~~~~~~~~~~~~~~~~~~~
run_namelist_dir=${WRK_DIR}/config/namelists
# parent information is taken from canesm.cfg file but can be overwritten below
runid_in_a=${override_restart_runid:-$parent_runid}     # Parent runid to be used as AGCM restart
date_in_a=${override_restart_time:-$parent_branch_time} # Restart date in the parent run for AGCM
runid_in_c=${override_restart_runid:-$parent_runid}     # Parent runid to be used as Coupler/NEMO restart
date_in_c=${override_restart_time:-$parent_branch_time} # Restart date in the parent run for Coupler/NEMO
# optional local database to access files from another run on the same machine:
# edit $USER of the input runid, if it comes from a different user.
DATAPATH_DB_OPT=$RUNPATH_ROOT/users/$USER/$runid_in_a/datapath_local.db
# check what restarts are needed and throw errors if user provided erroneous options for namelists
case $config in
    ESM) save_agcm_files=1; save_coupler_files=1; save_nemo_files=1;
         required_restarts="CanAM: mc_${runid_in_a}_${date_in_a} CanCPL: mc_${runid_in_c}_${date_in_c} CanNEMO: mc_${runid_in_c}_${date_in_c}";;
    AMIP) save_agcm_files=1; save_coupler_files=1; save_nemo_files=0;
          required_restarts="CanAM: mc_${runid_in_a}_${date_in_a} CanCPL: mc_${runid_in_c}_${date_in_c}";
          (( ${#nemo_restart_namelists_to_use[@]} )) && bail "The use of nemo namelists isn't valid for an AMIP run!" ;;
    OMIP) save_agcm_files=0; save_coupler_files=0; save_nemo_files=1;
          required_restarts="CanNEMO: mc_${runid_in_c}_${date_in_c}";
          (( ${#agcm_restart_namelists_to_use[@]} ))    && bail "The use of agcm namelists isn't valid for an OMIP run!";
          (( ${#coupler_restart_namelists_to_use[@]} )) && bail "The use of coupler namelists isn't valid for an OMIP run!" ;;
esac
# derive start_year and start_month
start_year=${run_segment_start_year} 
start_month=${run_segment_start_month} 
# derive date_out from start_year/start_month
if (( $start_month == 1 )) ; then
    year_out=$(( start_year - 1 ))
    year_out=$(pad_integer $year_out 4 )
    month_out=12
else
    year_out=$start_year
    month_out=$(( start_month - 1 ))
    month_out=$(pad_integer $month_out 2 )
fi
runid_out=${runid}                 # runid of your run
date_out=${year_out}_m${month_out} # restart date of your run
echo runid_in_a=${runid_in_a} date_in_a=${date_in_a}
echo runid_in_c=${runid_in_c} date_in_c=${date_in_c}
echo runid_out=${runid_out} date_out=${date_out}
DATAPATH_DB1=$DATAPATH_DB
# append optional local database from another run
if [ -n "$DATAPATH_DB_OPT" ] ; then
    DATAPATH_DB1="$DATAPATH_DB1:$DATAPATH_DB_OPT"
fi
# modify DATAPATH_DB to search for restart files in system-wide database on ppp
DATAPATH_DB1="$DATAPATH_DB1:$SITESTORE_ROOT/../ppp_data/database_dir/datapath_$(echo $CMCFEDEST | cut -f2 -d-).db"
echo $DATAPATH_DB1
# extract year from date_out
year_out=$(echo $date_out | cut -f1 -d'_')
month_out=$(echo $date_out | cut -f2 -d'_' | cut -c2-3)
# initial year
if (( month_out == 12 )) ; then
  iyear=$(( year_out + 1 ))
else
  iyear=$year_out
fi
echo year_out=$year_out month_out=$month_out iyear=$iyear

# compare restart months
month_in_a=$(echo $date_in_a | cut -f2 -d'_' | cut -c2-3)
month_in_c=$(echo $date_in_c | cut -f2 -d'_' | cut -c2-3)
if (( month_in_a != month_out )) || (( month_in_a != month_in_c )); then
    echo "month_in_a=$month_in_a month_in_c=$month_in_c month_out=$month_out"
    echo "ERROR in save_restart_files: Input and output restart months must be the same."
    exit 1
fi
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Retrieve, Process, and Save restart files
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# create a temp dir not to interfere with files in local directory
tmpdir=$RUNPATH/tmp.restart.$$
# create a trap to remove $tmpdir on exit
trap "cleanup" 0 1 2 3 6 15
cleanup()
{
    exit_status=$?
    echo exit_status=$exit_status
    echo "Caught Signal ... cleaning up."
    rm -rf $tmpdir
    echo "Done cleanup ... quitting."
    if (( exit_status != 0 )) ; then
        echo "*** ERROR in the save_restart_files script!!! ***"
        echo "Please verify input restart files are accessible:"
        echo "$required_restarts"
        echo "For access errors, see ${access_log_file}"
    fi
    exit $exit_status
}
mkdir -p $tmpdir
cd $tmpdir
if (( save_agcm_files == 1 )); then

    # get restart
    #--------------
    agcm_restart=mc_${runid_out}_${date_out}_${agcm_restart_identifier}
    env DATAPATH_DB=$DATAPATH_DB1 access ${agcm_restart} mc_${runid_in_a}_${date_in_a}_${agcm_restart_identifier} nocp=no na >> ${access_log_file} 2>&1
    if ! is_directory ${agcm_restart}; then
        # THIS IS FOR LEGACY RESTARTS AND SHOULD BE REMOVED ONCE ALL RESTARTS ARE IN NEW FORMAT

        # failed to get bundled format - this is an old restart so we retrieve each file and bundle
        mkdir ${agcm_restart}
        cd ${agcm_restart}

        # get RS (AGCM) file
        env DATAPATH_DB=$DATAPATH_DB1 access ${agcm_restart_identifier}_OLDRS mc_${runid_in_a}_${date_in_a}_rs_nmf nocp=no >> ${access_log_file} 2>&1

        # get TS (CTEM) file
        env DATAPATH_DB=$DATAPATH_DB1 access ${agcm_restart_identifier}_OLDTS mc_${runid_in_a}_${date_in_a}_ts_nmf nocp=no >> ${access_log_file} 2>&1

        # If explicitly asked for, we try to access the namelists from the parent runid 
        #   - note: if not asked for, we only pull in the _required_ files 
        if (( ${#agcm_restart_namelists_to_use[@]} )); then                     # if this array has elements
            for nl_file in "${agcm_restart_namelists_to_use[@]}"; do
                # need to convert to lower name for access to function properly
                nl_file_lower=$(echo $nl_file | tr '[:upper:]' '[:lower:]')
                nl_file_in_restart_bundle=${agcm_restart_identifier}_${nl_file}
                file_to_access="mc_${runid_in_a}_${date_in_a}_${nl_file_lower}"

                # attempt access
                env DATAPATH_DB=$DATAPATH_DB1 access $nl_file_in_restart_bundle $file_to_access nocp=no na >> ${access_log_file} 2>&1

                if ! is_file $nl_file_in_restart_bundle; then
                    echo "Failed to retrieve $nl_file from $runid_in_a! Will use the namelists that were already configured for this run."
                else
                    echo "Successfully pulled $nl_file from $runid_in_a!"
                fi
            done
        fi
        cd $tmpdir
    fi

    # prep files
    #-----------
    cd $agcm_restart

    # pack files if necessary
    if [[ $agcm32bit == off ]]; then
        # pack TS and RS file
        echo "Packing RS and TS file!"
        for file_type in OLDTS OLDRS; do
            pakrs_float1 ${agcm_restart_identifier}_${file_type} tmp_${file_type}
            rm -f ${agcm_restart_identifier}_${file_type}
            mv tmp_${file_type} ${agcm_restart_identifier}_${file_type}
        done
    fi

    # store restart namelist files and use for this run, if requested
    if (( ${#agcm_restart_namelists_to_use[@]} )); then           # if this array has elements
        for nl_file in "${agcm_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=${agcm_restart_identifier}_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in_a!"
            else
                echo "Using $nl_file from agcm restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $agcm_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in_a but its not contained in \$agcm_namelists!"
                fi
            fi
        done
    fi
    cd $tmpdir

    # save restart
    #--------------
    save $agcm_restart $agcm_restart
    release $agcm_restart
fi

if (( save_coupler_files == 1 )); then
    # get coupler restart bundle
    coupler_restart=mc_${runid_out}_${date_out}_cplrs
    env DATAPATH_DB=$DATAPATH_DB1 access ${coupler_restart} mc_${runid_in_c}_${date_in_c}_cplrs nocp=off >> ${access_log_file} 2>&1

    # if desired, pull namelists from restart and use for this run
    if (( ${#coupler_restart_namelists_to_use[@]} )); then
        cd $coupler_restart
        for nl_file in "${coupler_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=cplrs_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in_c!"
            else
                echo "Using $nl_file from coupler restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $coupler_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in_c but its not contained in \$coupler_namelists!"
                fi
            fi
        done
        cd $tmpdir
    fi

    # save
    save ${coupler_restart} ${coupler_restart}
    release ${coupler_restart}
fi

if (( save_nemo_files == 1 )) ; then
    # get NEMO restart bundle
    nemo_restart=mc_${runid_out}_${date_out}_nemors
    env DATAPATH_DB=$DATAPATH_DB1 access tmp_${nemo_restart} mc_${runid_in_c}_${date_in_c}_nemors nocp=off na >> ${access_log_file} 2>&1
    if ! exists tmp_${nemo_restart} ; then
        # file not found - try with .tar extension
        env DATAPATH_DB=$DATAPATH_DB1 access tmp_${nemo_restart} mc_${runid_in_c}_${date_in_c}_nemors.tar nocp=off >> ${access_log_file} 2>&1
    fi
    if ! is_directory tmp_${nemo_restart}; then
        # we got a tar file - extract
        echo "Accessed nemors is a tar file! Extracting contents into $nemo_restart"
        mkdir $nemo_restart
        cd $nemo_restart
        tar -xvf ../tmp_${nemo_restart} >> /dev/null 2>&1
        cd $tmpdir
    else
        # we got a directory, simply rename
        mv tmp_${nemo_restart} ${nemo_restart}
    fi

    # if desired, pull namelists from restart and use for this run
    if (( ${#nemo_restart_namelists_to_use[@]} )); then
        cd $nemo_restart
        for nl_file in "${nemo_restart_namelists_to_use[@]}"; do
            nl_file_in_restart_bundle=rs_${nl_file}
            if ! is_file $nl_file_in_restart_bundle; then
                echo "$nl_file requested from user but it isn't present in restart bundle!"
                echo "Unable to use $nl_file from $runid_in_c!"
            else
                echo "Using $nl_file from nemo restart bundle"
                cp -f $nl_file_in_restart_bundle ${run_namelist_dir}/${nl_file}

                # print warning if this file isn't in the agcm namelist file list
                if [[ $nemo_namelists != *${nl_file}* ]]; then
                    echo "WARNING: $nl_file was extracted from $runid_in_c but its not contained in \$nemo_namelists!"
                fi
            fi
        done
        cd $tmpdir
    fi

    # save
    save ${nemo_restart} ${nemo_restart}
    release $nemo_restart
fi

# save environment and jobstring
env > MODEL_ENV
save  MODEL_ENV mc_${runid_out}_${date_out}_env

cd $call_dir
