#!/bin/bash
#
# This is a simple shell sequencer designed to run CanESM in one year chunks.
# It assumes the input of certain variables. Typically these comes from
# files created by imsi, but they could come from elsewhere.
#
#
# TODO:
#   - The resubmission from the compute node does not work on all machines, e.g. niagara. Might need an ssh step. Annoyingly.
#   - How to handle access of possible rebuilt vs tiled nemors?

set -e
set -x

# Check required variables are defined
# there should be a function defined to just do this on a list of vars!
[[ ! -d "${CANESM_SRC_ROOT}" ]]     && ( echo "Required code CANESM_SRC_ROOT does not exist" ; exit 1 )
[[ -z "${RUNPATH}" ]]               && ( echo "RUNPATH must be defined" ; exit 1 )
[[ -z "${DATAPATH_DB}" ]]           && ( echo "DATAPATH_DB must be defined" ; exit 1 )
[[ -z "${WRK_DIR}" ]]               && ( echo "WRK_DIR must be defined"; exit 1 )
[[ -z "${CanAM_EXEC}" ]]            && ( echo "CanAM_EXEC must be defined"; exit 1 )
[[ -z "${CanCPL_EXEC}" ]]           && ( echo "CanAM_CPL must be defined"; exit 1 )

# Get useful CanESM shell functions. Is it still worth constructing an external environment file?
# Could be defined in configs if we can figure out internal references.
source $CANESM_SRC_ROOT/CCCma_tools/tools/CanESM_shell_functions.sh

# Get inputs
export year=$CURRENT_YEAR               # should not be necessary - make consistent use of time info everywhere!!!!
export LAST_YEAR=$(( CURRENT_YEAR -1 )) # should not be necessary - make consistent use of time info everywhere!!!!
export LAST_YEAR=$(echo $LAST_YEAR | awk '{printf "%04d",$1}')

source ${WRK_DIR}/config/imsi_get_input_files.sh

# Do model specific actions
source ${CANESM_SRC_ROOT}/imsi-config/lib/pre_canam_imsi.sh
source ${CANESM_SRC_ROOT}/imsi-config/lib/pre_cpl_imsi.sh

if [ "${model_config}" = "ESM" ] ; then
   source ${CANESM_SRC_ROOT}/imsi-config/lib/pre_nemo_imsi.sh
   nemo_end_step=`get_namelist_var nn_itend namelist` # this is required by directory packing below to know the RS to use
fi

# Run the model
# Create output files
for i in $(seq -f "%05g" 0 33)
do
  touch oe$i
  chmod a+r oe$i
done

# First, weak attempt at programatic construction. More like just a copy from machine_config, but can be improved.
# To improve it, programatically fill in things like size of each component.
${WRK_DIR}/config/imsi-model-run.sh && model_status=$? || model_status=$?

# This is the CCCma output naming convention (suboptimal)
run_prefix="mc_${runid}_"
mon=01
mon_rs=12
tail_suffix="_"

# model/restart names
model=$( printf "%s%04d_m%02d%s" $run_prefix $CURRENT_YEAR $mon $tail_suffix )
model_rs=$( printf "%s%04d_m%02d%s" $run_prefix $CURRENT_YEAR $mon_rs $tail_suffix )
echo model=$model
echo model_rs=$model_rs

# check things completed as expected and resubmit for the next chunk if necessary
if (( ${model_status} == 0 )) ; then

    echo "Finished run for ${CURRENT_YEAR}"

    source ${CANESM_SRC_ROOT}/imsi-config/lib/post_canam_imsi.sh
    source ${CANESM_SRC_ROOT}/imsi-config/lib/post_cpl_imsi.sh
    
    if [ "${model_config}" = "ESM" ] ; then
        source ${CANESM_SRC_ROOT}/imsi-config/lib/post_nemo_imsi.sh
    fi
    
    # copy the output files back
    source ${WRK_DIR}/config/imsi_directory_packing.sh

    export RUNPATH=${RUNPATH}/${CURRENT_YEAR}
    mkdir -p ${RUNPATH}
    source ${WRK_DIR}/config/imsi_save_output_files.sh

    # Increment CURRENT_YEAR. If <= $STOP_YEAR, resubmit
    NEXT_YEAR=$(( $CURRENT_YEAR + 1 ))
    
    if (( $NEXT_YEAR <= $END_YEAR )) ; then
        # update current year
        sed -i "s/CURRENT_YEAR=.*/CURRENT_YEAR=${NEXT_YEAR}/" ${WRK_DIR}/config/.simulation.time.state

        # Set launch state for next year
        sed -i "s/IMSI_RESUBMIT=.*/IMSI_RESUBMIT=TRUE/" ${WRK_DIR}/config/.simulation.time.state      
        echo "Resubmit for ${NEXT_YEAR}"
    else
        sed -i "s/IMSI_RESUBMIT=.*/IMSI_RESUBMIT=FALSE/" ${WRK_DIR}/config/.simulation.time.state
        echo "Run complete in ${CURRENT_YEAR}"
    fi

else
    bail "IT LOOKS LIKE THE MODEL CRASHED AND DIDN'T PROPERLY COMPLETE!"   
fi

next_year=$(( $CURRENT_YEAR + 1 ))
sed -i "s/CURRENT_YEAR=.*/CURRENT_YEAR=${next_year}/" ${WRK_DIR}/config/.simulation.time.state
sed -i "s/MODEL_RESUBMIT=.*/MODEL_RESUBMIT=FALSE/" ${WRK_DIR}/config/.simulation.time.state

# and will resubmit themselves until they are as close as possible to model time.                                                                                         
if [ "$next_year" -le "$END_YEAR" ] ; then
    echo "Resubmitting model for $NEXT_YEAR"
    sed -i "s/MODEL_RESUBMIT=.*/MODEL_RESUBMIT=TRUE/" ${WRK_DIR}/config/.simulation.time.state
fi