# Deal with creating a new restart
# most of this is useless renaming to follow existing conventions.
rm -f cplrs_cpl_restart.nc
mv cpl_restart_out.nc cplrs_cpl_restart.nc

cp -f cpl_atm_grid.nc cplrs_atm_grid.nc || :
cp -f cpl_ocn_grid.nc cplrs_ocn_grid.nc || :

mv ${CanCPL_EXEC} cplrs_${CanCPL_EXEC}
mv nl_coupler_par cplrs_nl_coupler_par