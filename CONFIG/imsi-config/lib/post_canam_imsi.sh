#!/bin/bash

# Rename restarts to stick to old convention 
# The list could come from a variable, but better, the useless renaming could be 
# avoided altogether with a minial refactor. RS files should ideally contain a counter in the name to be unique.
# For all other files, the fact that they are in the restart tells you there are "agcmrs" files. Renaming is not needed
# or beneficial in any obvious way other than that it was arbitrarily done for NEMO once years ago.

rm -f  OLDRS
mv NEWRS OLDRS
rm -f OLDTS
mv NEWTS OLDTS

agcm_restart_files="OLDRS OLDTS ${CanAM_EXEC} agcm.final.state modl.dat canam_settings"

for f in $agcm_restart_files; do
    rm -f agcmrs_${f}
    mv ${f} agcmrs_${f}
done

