# Clean old restarts
rm -rf OLDRS OLDTS

# Extract new restarts from incoming directory
cp -Lr AGCMRS_IN/agcmrs_OLDRS OLDRS
cp -Lr AGCMRS_IN/agcmrs_OLDTS OLDTS

# update counters
update_agcm_counters start_date=${chunk_start_YYYY}-${chunk_start_MM}-${chunk_start_DD} stop_date=${chunk_stop_YYYY}-${chunk_stop_MM}-${chunk_stop_DD} agcm_timestep=900 namelist_file=modl.dat

