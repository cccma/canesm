# Clean the working directory of any existing restart files
rm -rf cpl_restart.nc

# Unpack restart files from restart bundled directories:
cp -Lr CPLRS_IN/cplrs_cpl_restart.nc cpl_restart.nc
cp -Lr CPLRS_IN/cplrs*grid* .

# Update namelist counters
update_coupler_counters start_date=${chunk_start_YYYY}-${chunk_start_MM}-${chunk_start_DD} stop_date=${chunk_stop_YYYY}-${chunk_stop_MM}-${chunk_stop_DD} runid=$runid namelist_file=nl_coupler_par

