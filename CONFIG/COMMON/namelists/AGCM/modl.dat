! The following variables are related to the "critical parameters" for CanAM.
! Only the scalars are listed here.  The lists, for e.g. level information, are
! at the end of the file since they can get pretty long.

! Variables read in by ingcm
&INGCM
  ksteps=12,        ! number of time-steps between coupling cycles
  kstart=0,         ! final step counter for the previous run
  kfinal=999,       ! ending step counter for the AGCM
  agcm_rstctl=0,    ! controls how ingcm handles kstart comparison between the restart and this namelist
                    !   0 -> just outputs the input variables
                    !   1 -> if bails if there are inconsistencies
  newrun=0,
  levs1=49,         ! number of vertical moisture levels; for initialization run
  incd=1,
  jlatpr=0,
  delt=900.0,       ! AGCM time-step in secs
  ifdiff=0,
  icoordc="ET15",
  iyear=1850,
/

&agcm_sizes
  nlatd   = 96,         ! number of gaussian latitudes on the dynamics grid
  lonsld  = 192,        ! number of longitudes on the dynamics grid
  lond    = 192         ! number of grid points per slice for dynamics
  nlat    = 64,         ! number of gaussian latitudes on the physics grid
  lonsl   = 128,        ! number of longitudes on the physics grid
  lonp    = 128,        ! number of grid points per slice for physics
  ilev    = 49,         ! number of vertical model levels
  levs    = 49,         ! number of vertical moisture levels
  nnodex  = 32,         ! number of (smp?) nodes used to run the AGCM
  ilg     = 131
  ilgd    = 195
  lmtotal = 64
/
!  ntrac=16,             ! total number of tracers in the AGCM
!  ntraca=12,            ! number of advected tracers in the AGCM

! Other critical scalar parameters need to define the structure of CanAM
&CRIT_PARAM_CANAM_SCALAR
  lmt=63,               ! spectral truncation wave number
  lon=128,              ! number of grid points per slice for physics
  lay=2,
  coord=ET15,
  plid=50.0,
  iflm=1,               ! integer switch to control fractional land mask (1 -> on)
  nnode_a=32,           ! number of (smp?) nodes used to run the AGCM - this doesn't seem to be the right name.. it gets changed to NNODEX in the code
  NRFP=1
/

&agcm_options
  agcm_river_routing = .true.
  biogeochem = .true.
  cf_sites = .false.
  dynrhs = .false.
  isavdts = .false.
  myrssti = .false.
  parallel_io = .true.  ! =.true. means to use parallel I/O for AGCM model files. Set #define parallel_io in cppdefs file!
  relax = .false.
  timers = .false.
  use_canam_32bit = .true.
  rcm = .false.
/

! Parameters for relaxation scheme
&relax_parm
      !--- Set the relaxation parameters
      relax_spc = 1             ! spectral nudging
      relax_spc_bc = 0          ! spectral climatological nudging

      relax_tg1 = 0             ! Ground temperature level 1 nudging
      relax_tg1_bc = 0          ! Ground temperature level 1 climatological nudging

      relax_wg1 = 0             ! Ground moisture level 1 nudging
      relax_wg1_bc = 0          ! Ground moisture level 1 climatological nudging

      relax_ocn = 0             ! Upper ocean (SST/SIC/SICN) nudging
      relax_ocn_bc = 0          ! Upper ocean climatological nudging

      iperiod_spc=6             ! Spectral nudging cycle in hours.
      iperiod_lnd=24            ! Land nudging cycle in hours.
      iperiod_lnd_clm=6         ! Land climatological nudging cycle in hours.
      iperiod_ocn=24            ! Ocean nudging cycle in hours.
      iperiod_ocn_clm=24        ! Ocean climatological nudging cycle in hours.

      relax_p=1.                ! VORT vorticity relaxation switch.
      relax_c=1.                ! DIV  divergence relaxation switch.
      relax_t=1.                ! TEMP temperature relaxation switch.
      relax_es=1.               ! ES   humidity relaxation switch.
      relax_ps=0.               ! PS   ln(surface pressure) relaxation switch.

      tauh_p=24.                ! VORT relaxation time scale in hours
      tauh_c=24.                ! DIV  relaxation time scale in hours
      tauh_t=24.                ! TEMP relaxation time scale in hours
      tauh_es=24.               ! HUM  relaxation time scale in hours
      tauh_ps=24.               ! PS   relaxation time scale in hours

      tauh_tg1=72.              ! TG1 relaxation time scale in hours
      tauh_wg1=72.              ! WG1 relaxation time scale in hours

      ntrunc_p=21               ! truncation of VORT tendencies
      ntrunc_c=21               ! truncation of DIV  tendencies
      ntrunc_t=21               ! truncation of TEMP tendencies
      ntrunc_es=21              ! truncation of HUM  tendencies
      ntrunc_ps=21              ! truncation of PS   tendencies

      itrunc_tnd = 1            ! itrunc_tnd - (0/1) truncate tendency with truncf
      itrunc_anl = 0            ! trunc_anl - (0/1) truncate analysis data towards which model is nudged

      ivfp=0                    ! =0, no vertical profile to nudge everywhere with equal strength
                                ! =1, use vertical profile at the top to reduce nudging near the top
                                ! =2 vertical profile at the bottom to reduce nudging near the bottom
                                ! =3 vertical profile both at the top and bottom to reduce nudging at both ends

      sgtop=0.001               ! top full levels (0.001~1hPa)
      shtop=0.001               ! top half levels (0.001~1hPa)

      sgfullt_p=0.010           ! where to start reducing forcing (0.010~10hPa)
      sgfullt_c=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_t=0.010           ! where to start reducing forcing (0.010~10hPa)
      shfullt_e=0.010           ! where to start reducing forcing (0.010~10hPa)

      sgdampt_p=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      sgdampt_c=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
      shdampt_t=0.010           ! reduction factor from shfull to shtop (0.01=1%)
      shdampt_e=0.010           ! reduction factor from shfull to shtop (0.01=1%)

      sgbot=0.995               ! bottom full levels (0.995~995hPa)
      shbot=0.995               ! bottom half levels (0.995~995hPa)
      sgfullb_p=0.850           ! where to start reducing forcing (0.850~850hPa)
      sgfullb_c=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_t=0.850           ! where to start reducing forcing (0.850~850hPa)
      shfullb_e=0.850           ! where to start reducing forcing (0.850~850hPa)

      sgdampb_p=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      sgdampb_c=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
      shdampb_t=0.010           ! reduction factor from shfulb to shbot (0.01=1%)
      shdampb_e=0.010           ! reduction factor from shfulb to shbot (0.01=1%)

      tauh_sst=72.              ! in hours - nudging time scale for SSTs
      tauh_edge=72.             ! in hours - nudging time scale for sea-ice mass (SIC) near edge
                                ! may use shorter time scale since it is better observed
      tauh_sic=168.             ! in hours - nudging time scale for sea-ice mass (SIC)

      rlx_sst=1.                ! convert delta sst to heat tendency
      rlx_sic=1.                ! convert delta sic to heat tendency

      rlx_tbeg=1.               ! turn on/off (1/0) tbeg heat tendency
      rlx_tbei=1.               ! turn on/off (1/0) tbei heat tendency
      rlx_tbes=1.               ! turn on/off (1/0) tbes heat tendency

      rlc_tbeg=0.               ! turn on/off (1/0) tbeg heat clim. tendency
      rlc_tbei=0.               ! turn on/off (1/0) tbei heat clim. tendency
      rlc_tbes=0.               ! turn on/off (1/0) tbes heat clim. tendency

      rlx_sbeg=1.               ! turn on/off (1/0) sbeg heat tendency
      rlx_sbei=1.               ! turn on/off (1/0) sbei heat tendency
      rlx_sbes=1.               ! turn on/off (1/0) sbes heat tendency

      rlc_sbeg=0.               ! turn on/off (1/0) sbeg heat clim. tendency
      rlc_sbei=0.               ! turn on/off (1/0) sbei heat clim. tendency
      rlc_sbes=0.               ! turn on/off (1/0) sbes heat clim. tendency
/

! Details about the vertical layer information (from the critical parameters)

! Momentum levels (note that some of the upper levels are encoded)
! Empty values are present since the code is writen such that there
! is current a maximum array size of 100 for the level information
&CANAM_LEVEL_MOMENTUM
  g(1)=-0108,
  g(2)=-0144,
  g(3)=-0190,
  g(4)=-0250,
  g(5)=-0327,
  g(6)=-0426,
  g(7)=-0550,
  g(8)=-0708,
  g(9)=-0904,
  g(10)=99115,
  g(11)=99145,
  g(12)=99182,
  g(13)=99228,
  g(14)=99283,
  g(15)=99350,
  g(16)=99430,
  g(17)=99525,
  g(18)=99637,
  g(19)=99769,
  g(20)=99923,
  g(21)=110,
  g(22)=131,
  g(23)=154,
  g(24)=181,
  g(25)=211,
  g(26)=245,
  g(27)=283,
  g(28)=325,
  g(29)=370,
  g(30)=420,
  g(31)=474,
  g(32)=531,
  g(33)=592,
  g(34)=648,
  g(35)=698,
  g(36)=742,
  g(37)=780,
  g(38)=813,
  g(39)=841,
  g(40)=865,
  g(41)=885,
  g(42)=902,
  g(43)=916,
  g(44)=930,
  g(45)=944,
  g(46)=958,
  g(47)=972,
  g(48)=986,
  g(49)=995,
  g(50)=0,
  g(51)=0,
  g(52)=0,
  g(53)=0,
  g(54)=0,
  g(55)=0,
  g(56)=0,
  g(57)=0,
  g(58)=0,
  g(59)=0,
  g(60)=0,
  g(61)=0,
  g(62)=0,
  g(63)=0,
  g(64)=0,
  g(65)=0,
  g(66)=0,
  g(67)=0,
  g(68)=0,
  g(69)=0,
  g(70)=0,
  g(71)=0,
  g(72)=0,
  g(73)=0,
  g(74)=0,
  g(75)=0,
  g(76)=0,
  g(77)=0,
  g(78)=0,
  g(79)=0,
  g(80)=0,
  g(81)=0,
  g(82)=0,
  g(83)=0,
  g(84)=0,
  g(85)=0,
  g(86)=0,
  g(87)=0,
  g(88)=0,
  g(89)=0,
  g(90)=0,
  g(91)=0,
  g(92)=0,
  g(93)=0,
  g(94)=0,
  g(95)=0,
  g(96)=0,
  g(97)=0,
  g(98)=0,
  g(99)=0,
  g(100)=0,
/

! Thermodynamic levels (note that some of the upper levels are encoded)
&CANAM_LEVEL_THERMO
  h(1)=-0108,
  h(2)=-0144,
  h(3)=-0190,
  h(4)=-0250,
  h(5)=-0327,
  h(6)=-0426,
  h(7)=-0550,
  h(8)=-0708,
  h(9)=-0904,
  h(10)=99115,
  h(11)=99145,
  h(12)=99182,
  h(13)=99228,
  h(14)=99283,
  h(15)=99350,
  h(16)=99430,
  h(17)=99525,
  h(18)=99637,
  h(19)=99769,
  h(20)=99923,
  h(21)=110,
  h(22)=131,
  h(23)=154,
  h(24)=181,
  h(25)=211,
  h(26)=245,
  h(27)=283,
  h(28)=325,
  h(29)=370,
  h(30)=420,
  h(31)=474,
  h(32)=531,
  h(33)=592,
  h(34)=648,
  h(35)=698,
  h(36)=742,
  h(37)=780,
  h(38)=813,
  h(39)=841,
  h(40)=865,
  h(41)=885,
  h(42)=902,
  h(43)=916,
  h(44)=930,
  h(45)=944,
  h(46)=958,
  h(47)=972,
  h(48)=986,
  h(49)=995,
  h(50)=0,
  h(51)=0,
  h(52)=0,
  h(53)=0,
  h(54)=0,
  h(55)=0,
  h(56)=0,
  h(57)=0,
  h(58)=0,
  h(59)=0,
  h(60)=0,
  h(61)=0,
  h(62)=0,
  h(63)=0,
  h(64)=0,
  h(65)=0,
  h(66)=0,
  h(67)=0,
  h(68)=0,
  h(69)=0,
  h(70)=0,
  h(71)=0,
  h(72)=0,
  h(73)=0,
  h(74)=0,
  h(75)=0,
  h(76)=0,
  h(77)=0,
  h(78)=0,
  h(79)=0,
  h(80)=0,
  h(81)=0,
  h(82)=0,
  h(83)=0,
  h(84)=0,
  h(85)=0,
  h(86)=0,
  h(87)=0,
  h(88)=0,
  h(89)=0,
  h(90)=0,
  h(91)=0,
  h(92)=0,
  h(93)=0,
  h(94)=0,
  h(95)=0,
  h(96)=0,
  h(97)=0,
  h(98)=0,
  h(99)=0,
  h(100)=0,
/
