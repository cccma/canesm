from abc import ABC, abstractmethod

class platform(ABC):
  """Holds platform specific configuration options

  :param name: The name of the platform (e.g. banting, google-compute)
  :type name: string
  :param data_directory: The root path where inputs for CanESM experiments are stored
  :type data_directory: string
  :param experiments: A list of experiments to test on this platform
  :type  experiments: list
  :param compilers: Dictionary containing the compilers on this platform
  :type compilers: dict

  :type ABC: [type]
  """
  name = 'generic'
  data_directory = None
  nemo_arch = None
  mkmf_template = None
  make_cmd = 'make'
  temp_directory = '/tmp/'
  env_vars_run = {}

  def __init__(self, canesm_source, num_atmosphere_nodes, num_ocean_ranks):
    self.canesm_source = canesm_source
    self.coupler_source = f'{canesm_source}/CanCPL'
    self.atmosphere_source = f'{canesm_source}/CanAM'
    self.ocean_source = f'{canesm_source}/CanNEMO'
    self.config_dir = f'{canesm_source}/CONFIG'
    self.num_atmosphere_nodes = num_atmosphere_nodes
    self.num_ocean_ranks = num_ocean_ranks

class ubuntu_container(platform):
  """Defines all platform-specific options for
     running an Ubuntu 18.04 container using GNU compilers
  """

  name = 'ubuntu_container'
  data_directory = '/mnt/canesm-inputs/'
  compiler_toolchain = 'gnu'

  def __init__(self, *args):
    super().__init__(*args)

supported_platforms = {
  'ubuntu_container':ubuntu_container
}
